# Fast Fourier Transform
##### Please checkout following branch for latest stable code:
* dev

# Branch Structure:
* master: empty, initial project setup.
* dev: stable development branch.
* other: local and individual work.